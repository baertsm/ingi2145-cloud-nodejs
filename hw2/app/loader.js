var fs = require('fs');
var byline = require('byline');
var mongo = require('mongodb').MongoClient;
var assert = require('assert');
var AM = require('./account-manager.js');

mongo.connect('mongodb://localhost:27017/twitter', function(err, db) {

  assert.equal(null, err);
  var lineCountUsers     = 0;
  var lineCountSample    = 0;
  var lineCountFollowers = 0;
  var readAllLines = false;

  // load DB
  users_db     = db.collection('users');
  tweets_db    = db.collection('tweets');
  followers_db = db.collection('followers');

  // empty the databases
  var noop = function() {};
  users_db.remove({}, noop);
  tweets_db.remove({}, noop);
  followers_db.remove({}, noop);

  // Two files to be read
  var semaphore = 2;
  function callback(err) {
    --semaphore;
    console.log('End for ' + semaphore);
    if (semaphore !== 0)
      return;
    // this callback is the last one which is called.
    if (lineCountUsers === 0 && lineCountSample === 0 && lineCountFollowers === 0) {
      console.log('Close DB');
      db.close();
    }
    else
      readAllLines = true;
  }

  // add test user
  lineCountUsers++;
  AM.addNewAccount(
    { username: 'test', name: 'Test Account', following: [] },
    users_db, null, function() { lineCountUsers--; }, function() {});

  var u = byline(fs.createReadStream(__dirname + '/users.json'));

  u.on('data', function(line) {
    try {
      lineCountUsers++;
      var obj = JSON.parse(line);
      // console.log(obj);
      // NOTE: obj represents a user and contains three fields:
      // obj.username: the username
      // obj.name: the full name
      // obj.followers: array of users following this user
      // obj.following: array of followed users
      lineCountFollowers += obj.following.length;

      AM.addNewAccount(obj, users_db, followers_db, function() {
        if (--lineCountUsers === 0 && lineCountFollowers === 0 && readAllLines) {
          console.log('Close DB (end: Users)');
          db.close();
        }
      }, function() {
        if (--lineCountFollowers === 0 && lineCountUsers === 0 && readAllLines) {
          console.log('Close DB (end: Followers)');
          db.close();
        }
      });
    } catch (err) {
      console.log("Error:", err);
    }
  });
  u.on('end', callback);


  var t = byline(fs.createReadStream(__dirname + '/sample.json'));

  t.on('data', function(line) {
    try {
      lineCountSample++;
      var obj = JSON.parse(line);
      // console.log(obj);
      // NOTE: obj represents a tweet and contains three fields:
      // obj.created_at: UTC time when this tweet was created
      // obj.text: The actual UTF-8 text of the tweet
      // obj.username: The user who posted this tweet
      // obj.name: The fullname of the user who posted this tweet

      AM.addNewTweet(obj, tweets_db, function() {
        if (--lineCountSample === 0 && readAllLines) {
          console.log('Close DB (end: Tweets)');
          db.close();
        }
      });
    } catch (err) {
      console.log("Error:", err);
    }
  });
  t.on('end', callback);


});
