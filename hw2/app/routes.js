var express = require('express');
var assert = require('assert');
var app = require('./app.js');
var ejs = require('ejs');
var fs = require('fs');
var AM = require('./account-manager.js');
var router = express.Router();


////////////////////////////////////////////////////////////////////////////////

function setDisplayDate(tweetsToDisplay) {
  tweetsToDisplay.forEach(function(tweet) {
    tweet.display_time = new Date(tweet.created_at).toString();
  });
  return tweetsToDisplay;
}

function notConnectedRedirect(req, res, me) {
  if (me == null) {
    // if user is not logged in redirect back to login page
    if (!req.cookies.user) {
      res.redirect('/'); // or autologin
      return true;
    }
    // kind of hack, just for tests: if cookies, set user + refresh
    AM.autoLogin(
      req.cookies.user.username,
      req.cookies.user.pass,
      app.users,
      function(user) {
        if (!user)
          res.redirect('/');
        else {
          req.session.user = user;
          res.redirect(req.get('referer')); // refresh
        }
      }
    );
    return true;
  }
  return false;
}

// NOTE(norswap): This method is necessary because, annoyingly, EJS does not
//  support dynamic includes (including a file whose name is passed via the
//  dictionary -- it has to be hardcoded in the template instead).
function render(res, dict) {
  fs.readFile('views/'+ dict.partial + '.ejs', 'utf-8', function(err, data) {
    assert(!err);
    dict.partial = ejs.render(data, dict);
    res.render('template', dict);
  });
}

////////////////////////////////////////////////////////////////////////////////
// Login page

router.get('/', function(req, res) {
  // From lab7
  // check if the user's credentials are saved in a cookie
  if (!req.session.user) {
    if (req.cookies.user)
      req.session.user = req.cookies.user; // yeah, we should just use user/pass
    else
      return res.render('login', {
        title: 'Hello - Please Login To Your Account' });
  }

  // attempt automatic login
  AM.autoLogin(
    req.session.user.username,
    req.session.user.pass,
    app.users,
    function(user) {
      if (!user) {
        console.log("Wrong user/password")
        return res.render('login', {
          title: 'Please Login To Your Account' });
      }

      req.session.user = user;
      res.redirect('/home');
    }
  );
});

////////////////////////////////////////////////////////////////////////////////
// Login / Logout

router.post('/validate', function(req, res) {
  // from Lab7
  AM.manualLogin(
    req.body.username,
    req.body.password,
    app.users,
    function(err, user) {
      //assert(!err);
      res.setHeader('content-type', 'application/json');

      if (!user) {
        res.statusCode = 403;
        var o = {message: err.message};
        res.send(JSON.stringify(o));
        return;
      }

      var year = 31536000000;
      res.cookie('user', user, { expires: new Date(Date.now() + year), maxAge: year }); // save all (yeah, maybe too much but ok for now)

      req.session.user = user;
      var fullUrl = req.protocol + '://' + req.get('host') + '/home';
      var o = {message: 'OK', url: fullUrl}
      res.send(JSON.stringify(o));
    }
  );
});

router.post('/logout', function(req, res) {
  req.session.destroy();
  res.clearCookie('user');
  res.redirect('/');
});

////////////////////////////////////////////////////////////////////////////////
// User Profile

function render_usr(res, display_user, username, display_f, following, tweets) {
  render(res, {
    title: display_user ? 'Last Tweets of ' + display_user : 'User not found',
    username: username,
    display_f: display_f,
    following: following,
    partial: 'profile',
    tweets: setDisplayDate(tweets)
  });
}

router.get('/usr/:username', function(req, res) {
  var me = req.session.user;
  if (notConnectedRedirect(req, res, me))
    return;

  var username = req.params.username;
  // find this requested user
  app.users.find({ username: username })
    .toArray(function(err, user) {
      // user not found
      if (err || !user || user.length === 0) {
        console.log('User not found ' + err);
        render_usr(res, false, username, false, false, []);
      }
      else {
        // find all tweets of this user
        app.tweets.find({ username: username })
          .sort({ created_at: -1 })
          .limit(10)
          .toArray(function(err, array) {
            if (err || !array) {
              console.log(err);
              array = []; // or return? better: a message to the user
            }
            // if the same user: not display (un)follow
            if (me.username === username) {
              console.log('Hide follow');
              render_usr(res, user[0].name, username, false, false, array);
            }
            else {
              // check following
              app.followers.count({ username: me.username, follows: username },
                function(err, count) {
                  render_usr(res, user[0].name, username, true, !err && count !== 0, array);
                });
              }
            }
          );
        }
      }
    );
});

router.get('/usr/:username/following', function(req, res) {
  var me = req.session.user;
  if (notConnectedRedirect(req, res, me))
    return;

  var username = req.params.username;
  app.followers.find({ username: username }, {follows: 1, _id: 0})
    .sort({ follows: +1 })
    .toArray(function(err, array) {
      if (err || !array) {
        console.log(err);
        array = []; // or return? better: a message to the user
      }
      var following = [];
      for (i in array)
        following.push(array[i].follows)

      render(res, {
        title: username + ' follows:',
        partial: 'follow',
        follow: following
      });
    }
  );
});

router.get('/usr/:username/followers', function(req, res) {
  var me = req.session.user;
  if (notConnectedRedirect(req, res, me))
    return;

  var username = req.params.username;
  app.followers.find({ follows: username }, {username: 1, _id: 0})
    .sort({ username: +1 })
    .toArray(function(err, array) {
      if (err || !array) {
        console.log(err);
        array = []; // or return? better: a message to the user
      }
      var followers = [];
      for (i in array)
        followers.push(array[i].username)

      render(res, {
        title: username + ' is followed by:',
        partial: 'follow',
        follow: followers
      });
    }
  );
});

// NOTE: I didn't modified the skeleton (as requested) but in the instruction, we should have a post request.
router.get('/usr/:username/follow', function(req, res) {
  var me = req.session.user;
  if (notConnectedRedirect(req, res, me))
    return;

  var username = req.params.username;
  app.followers.count({ username: me.username, follows: username },
    function(err, count) {
      if (err || count !== 0) {
        console.log ('follow: err ' + err + ' ' + count);
        res.redirect('back');
        return;
      }
      app.followers.insert(
        { username: me.username, follows: username },
        { safe: true },
        function(err, data) {
          console.log('err: ' + err);
          res.redirect('back');
        }
      );
    }
  );
});

router.get('/usr/:username/unfollow', function(req, res) {
  var me = req.session.user;
  if (notConnectedRedirect(req, res, me))
    return;

  var username = req.params.username;
  app.followers.count({ username: me.username, follows: username },
    function(err, count) {
      if (err || count === 0) {
        console.log ('unfollow: err ' + err + ' ' + count);
        res.redirect('back');
        return;
      }
      app.followers.remove(
        { username: me.username, follows: username },
        { safe: true },
        function(err, data) {
          console.log('err: ' + err);
          res.redirect('back');
        }
      );
    }
  );
});

////////////////////////////////////////////////////////////////////////////////
// User Timeline

router.get('/home', function(req, res) {
  if (notConnectedRedirect(req, res, req.session.user))
    return;
  console.log("Welcome Home");

  // app.tweets.find().sort( {created_at: -1} ).limit(10)
  app.followers.find({username: req.session.user.username}, {follows: 1, _id: 0}) // all people followed by this user
  .toArray(function(err, array){
    if (err) {
      console.log(err);
      array = []; // or return? better: a message to the user
    }
    followers = [req.session.user.username]; // it seems Tweeter adds users' tweets
    for (i in array)
      followers.push(array[i].follows);

    // find all related tweets with one DB instruction (for scability)
    app.tweets.find({ username: { $in : followers } })
      .sort({ created_at: -1 })
      .limit(10)
      .toArray(function(err, array) {
      if (err){
        console.log(err);
        array = []; // or return? better: a message to the user
      }
      render(res, {
        title: 'Last Tweets',
        partial: 'home',
        tweets: setDisplayDate(array)
      });
    });
  });
});

////////////////////////////////////////////////////////////////////////////////
// User Timeline
router.post('/newTweet', function(req, res) {
  if (notConnectedRedirect(req, res, req.session.user))
    return;

  console.log('new tweet: ' + req.body);
  var tweet = req.body.tweet;
  if (tweet !== "") {
    var result = {};
    res.setHeader('content-type', 'application/json');

    if (tweet.length > 140) {
      result.message = "Tweet too long";
      res.send(JSON.stringify(result));
    }
    else {
      newData = { text: tweet, username: req.session.user.username };
      AM.addNewTweet(newData, app.tweets, function() {
        result.message = "New tweet sent";
        res.send(JSON.stringify(result));
      });
      if (app.WITH_KAFKA) {
        app.producer.send(tweet, function (err, data) {
          console.log(data);
        });
      }
    }
  }
});

////////////////////////////////////////////////////////////////////////////////

module.exports = router;
