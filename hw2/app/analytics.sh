#!/bin/sh
# should be launch from the app dir:
#   ./analytics.sh [192.168.1.2]

DIR=`pwd`

# kafka
if [ "$1" == "" ]; then
    cd ~
    [ ! -d kafka_2.8.0-0.8.1.1 ] && tar xzf kafka_2.8.0-0.8.1.1.tgz
    cd kafka_2.8.0-0.8.1.1

    bin/zookeeper-server-start.sh config/zookeeper.properties > ../zookeeper.log 2> ../zookeeper.err &
    bin/kafka-server-start.sh config/server.properties > ../kafka.log 2> ../kafka.err &
fi

cd ~
[ ! -d spark-1.1.0-bin-hadoop2.4 ] && tar xzf spark-1.1.0-bin-hadoop2.4.tgz
cd "$DIR"/../spark-analytics
mvn package
mvn run &
echo "Wait 15 seconds"
sleep 15

# app
cd "$DIR"
# enable kafka
sed -i 's/app\.WITH_KAFKA = false/app.WITH_KAFKA = true/g' app.js
# connect to external kafka server
[ "$1" != "" ] && sed -i "s/localhost:2181/$1:2181" app.js

npm start

sed -i 's/app\.WITH_KAFKA = true/app.WITH_KAFKA = false/g' app.js
[ "$1" != "" ] && sed -i "s/$1:2181/localhost:2181" app.js
