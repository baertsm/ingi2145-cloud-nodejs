var crypto = require('crypto');
var moment = require('moment');

/* login validation methods */

exports.autoLogin = function(user, pass, accounts, callback)
{
  accounts.findOne({ username: user }, function(err, user) {
    if (err || !user)
      return callback(null);

    user.pass == pass ? callback(user) : callback(null);
  });
}

exports.manualLogin = function(user, pass, accounts, callback)
{
  accounts.findOne({ username: user }, function(err, user) {
    if (err || !user)
      return callback(
        new Error('wrong user/password'),
        null);

    validatePassword(pass, user.pass, function(err, res) {
      if (res) {
        callback(null, user);
      }
      else {
        callback(
          new Error('wrong user/password'),
          null);
      }
    });
  });
}

/* record insertion, update & deletion methods */

exports.addNewAccount = function(newData, users_db, followers_db, callback_users, callback_followers)
{
  /* the user should not exist
   * NOTE: this part could be avoid by using the username to create the ObjectID
   *       But if we have to keep this skeleton, I guess we shouldn't do that.
   */
  users_db.findOne({ username: newData.username }, function(err, user) {
    if (user) {
      callback_users('username-taken');
    }
    else {
      // insert users
      saltAndHash(newData.username, function(hash) {
        // user with username, name, pass, date
        var newUser = { username: newData.username, name: newData.name };
        // NOTE: This creates an account where the password is the username
        newUser.pass = hash;
        // append date stamp when record was created
        newUser.date = moment().format('MMMM Do YYYY, h:mm:ss a');
        // ('MMM DD HH:mm:ss Z YYYY');

        users_db.insert(newUser, {safe: true}, callback_users);
      });

      // insert users followed by this one
      for (var i = 0; i < newData.following.length; i++)
        followers_db.insert( {username: newData.username, follows: newData.following[i]}, {safe: true}, callback_followers);
    }
  });
}

/* insert new tweeter */

exports.addNewTweet = function(newData, tweets_db, callback)
{
  // no need to check if the tweet exists
  delete newData.name;
  if (newData.created_at == null)
    newData.created_at = moment().format('MMM DD HH:mm:ss Z YYYY');
  tweets_db.insert(newData, {safe: true}, callback);
}

/* private encryption & validation methods */

var generateSalt = function()
{
  var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
  var salt = '';
  for (var i = 0; i < 10; i++) {
    var p = Math.floor(Math.random() * set.length);
    salt += set[p];
  }
  return salt;
}

var md5 = function(str) {
  return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
  var salt = generateSalt();
  callback(salt + md5(pass + salt));
}

var validatePassword = function(plainPass, hashedPass, callback)
{
  var salt = hashedPass.substr(0, 10);
  var validHash = salt + md5(plainPass + salt);
  callback(null, hashedPass === validHash);
}
