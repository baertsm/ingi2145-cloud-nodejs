import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.streaming.*;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.kafka.*;
import java.util.Arrays;
import java.util.*;
import scala.Tuple2;

public class TwitterStreaming {
    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.err.println("Usage: TwitterStreaming <zkQuorum> <group> <topics>");
            System.exit(1);
        }
        // Location of the Spark directory
        String sparkHome = "/home/vagrant/spark-1.1.0-bin-hadoop2.4";
        // URL of the Spark cluster
        String sparkUrl = "local[4]";
        // Location of the required JAR files
        String jarFile = "target/streaming-1.0.jar";

        // inspired by Lab4
        // Generating spark's streaming context
        JavaStreamingContext ssc = new JavaStreamingContext(
        sparkUrl, "Streaming", new Duration(1000), sparkHome, new String[]{jarFile});
        // Obtain a set of tweets
        int numThreads = 4;
        Map<String, Integer> topicMap = new HashMap<String, Integer>();
        String[] topics = args[2].split(",");
        for (String topic: topics) {
            topicMap.put(topic, numThreads);
        }
        JavaPairReceiverInputDStream<String, String> tuples =
            KafkaUtils.createStream(jssc, args[0], args[1], topicMap);

        // Reduce hashtag maps to aggregate their counts in a sliding window fashion
        // reduceByKeyAndWindow counts hashtags in a 5-minute window that shifts every second
        JavaPairDStream<String, Integer> counts = tuples.reduceByKeyAndWindow(
            new Function2<Integer, Integer, Integer>() {
                public Integer call(Integer i1, Integer i2) { return i1 + i2; }
            },
            new Function2<Integer, Integer, Integer>() {
                public Integer call(Integer i1, Integer i2) { return i1 - i2; }
            },
            new Duration(60 * 5 * 1000),
            new Duration(1 * 1000)
        );

        //Swap the key-value pairs for the counts (in order to sort hashtags by their counts)
        JavaPairDStream<Integer, String> swappedCounts = counts.mapToPair(
            new PairFunction<Tuple2<String, Integer>, Integer, String>() {
                public Tuple2<Integer, String> call(Tuple2<String, Integer> in) {
                    return in.swap();
                }
            }
        );

        //Sort swapped map from highest to lowest
        JavaPairDStream<Integer, String> sortedCounts = swappedCounts.transformToPair(
            new Function<JavaPairRDD<Integer, String>, JavaPairRDD<Integer, String>>() {
                public JavaPairRDD<Integer, String> call(JavaPairRDD<Integer, String> in) throws Exception {
                    return in.sortByKey(false);
                }
            }
        );

        //Print top 10 hashtags
        sortedCounts.foreach(
            new Function<JavaPairRDD<Integer, String>, Void> () {
                public Void call(JavaPairRDD<Integer, String> rdd) {
                    String out = "\nTop 10 hashtags:\n";
                    for (Tuple2<Integer, String> t: rdd.take(10)) {
                        out = out + t.toString() + "\n";
                    }
                    System.out.println(out);
                    return null;
                }
            }
        );
        ssc.checkpoint("checkpoints");
        ssc.start();
    }
}
